package com.zipkin.usuariocreate.service;


import com.zipkin.usuariocreate.model.Usuario;
import com.zipkin.usuariocreate.repository.UsuarioRepository;
import com.zipkin.usuariocreate.viacep.ViaCepRetorno;
import com.zipkin.usuariocreate.viacep.ViaCepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private ViaCepService viaCepService;

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario create(String nome, String cep) {

        ViaCepRetorno viaCepRetorno = viaCepService.buscar(cep);

        Usuario usuario = new Usuario();
        usuario.setNome(nome);
        usuario.setLogradouro(viaCepRetorno.getLogradouro());

        return usuarioRepository.save(usuario);
    }

}