package com.zipkin.usuariocreate.repository;

import com.zipkin.usuariocreate.model.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {


}
