package com.zipkin.usuariocreate.viacep;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "viacep", configuration = ViaCepConfiguration.class)
public interface ViaCepService {

    @GetMapping("/viacep/{cep}")
    ViaCepRetorno buscar(@PathVariable String cep);
}
