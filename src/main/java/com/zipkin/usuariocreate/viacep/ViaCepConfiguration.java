package com.zipkin.usuariocreate.viacep;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ViaCepConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ViaCepErrorDecoder();
    }
}
