package com.zipkin.usuariocreate.viacep;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ViaCepErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new RuntimeException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
