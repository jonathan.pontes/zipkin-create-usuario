package com.zipkin.usuariocreate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class UsuariocreateApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsuariocreateApplication.class, args);
	}

}
