package com.zipkin.usuariocreate.controller;

import com.zipkin.usuariocreate.model.Usuario;
import com.zipkin.usuariocreate.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping("/cadastrar/{nome}/{cep}")
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario cadastrarUsuario(@PathVariable String nome,@PathVariable String cep){

        return usuarioService.create(nome, cep);
    }
}
